﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.Window;
using System.Threading;

namespace BS
{
    class Display : RenderWindow
    {

        private string message;

        public Display(VideoMode video, string title) : base(video, title)
        {

        }

        public void setMessage(string message)
        {
            this.message = message;
        }

        public void endGame(int player)
        {
            //base.Clear(new Color(100, 149, 237));
            Font myFont = new Font(@"images\BUXTONSKETCH.TTF"); 
            Text endText = new Text("Game Over, Player " + player + " wins.", myFont, 50);
            endText.Position = new Vector2f(400f, 100f);
            base.Draw(endText);
            base.Display();


            Thread.Sleep(2000);
            
        }

        public void sankMy(int ship)
        {
             switch (ship)
            {
                case 3:
                    this.message = "The Battle Ship has been sunk.";
                    break;
                case 4:
                case 5:
                    this.message = "The Destroyer has been sunk.";
                    break;
                case 6:
                case 7:
                case 8:
                    this.message = "The Submarine has been sunk.";
                    break;
                default:
                    this.message = "The Patrol Boat has been sunk.";
                    break;


            }





  //          Thread.Sleep(2000);
        }

        public void displayMessage()
        {

            Font myFont = new Font(@"images\BUXTONSKETCH.TTF");
            Text endText = new Text(this.message, myFont, 50);
            endText.Position = new Vector2f(400f, 100f);
            base.Draw(endText);

         }

        /*
         * This loads the textures for the gameboard of each player.
         * this uses most of the RenderWindow class to be able to call the draw method and display on the current board
         * without having to pass to many values.
         * It loads the pieces and will display them on the board.
         * */
        public void showBoard(GameBoard theBoard, int player)
        {
            //base.Clear(new Color(100, 149, 237));

            Sprite water = new Sprite(new Texture(@"images\water.png"));
            Sprite splash = new Sprite(new Texture(@"images\splash.png"));
            Sprite hit = new Sprite(new Texture(@"images\hit.png"));
            Sprite ship = new Sprite(new Texture(@"images\ship.png"));

            player *= 740;
            for( int x = 0; x < 10; x++)
                for (int y = 0; y < 10; y++)
                {
                    switch (theBoard.boardValue(x, y))
                    {
                            // 0 = unknown water
                            // 1 = splash
                            // 2 = hit
                            // 3 = ship
                        case 0:
                            water.Position = new Vector2f(x * 55f +player, y * 55f + 180);
                            base.Draw(water);
                            break;
                        case 1:
                            splash.Position = new Vector2f(x * 55f + player, y * 55f + 180);
                            base.Draw(splash);
                            break;
                        case 2:
                            hit.Position = new Vector2f(x * 55f + player, y * 55f + 180);
                            base.Draw(hit);
                            break;
                        default:
                            if (player > 0)  //if it is the enemy do not display. >0
                            {
                                water.Position = new Vector2f(x * 55f + player, y * 55f + 180);
                                base.Draw(water);
                            }
                            else
                            {
                                ship.Position = new Vector2f(x * 55f + player, y * 55f + 180);
                                base.Draw(ship);
                            }
                            break;
                    }
                }
             
        }
    }
}
