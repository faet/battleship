﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BS
{
    class GameBoard
    {
        private int[,] theBoard;
        private int thePlayer;
        
        //Makes random more random.
        private static readonly Random random = new Random();


        public GameBoard(int player)
        {
            this.thePlayer = player;

            theBoard = new int[10, 10];
            //if (player == 1)
            this.generateBoard();

        }

        private void generateBoard()
        {
            
            int randomX = random.Next(0, 10);
            int randomY = random.Next(0, 10);
            int randomDir = random.Next(0, 2);
            int boatNum = 3;

            while (!this.tryPlace(randomX, randomY, randomDir, 5, boatNum))
            {
                randomX = random.Next(0, 10);
                randomY = random.Next(0, 10);
                randomDir = random.Next(0, 2);
            }

            for (int destroyer = 0; destroyer < 2; destroyer++)
            {
                boatNum++;
                while (!this.tryPlace(randomX, randomY, randomDir, 4, boatNum))
                {
                    randomX = random.Next(0, 10);
                    randomY = random.Next(0, 10);
                    randomDir = random.Next(0, 2);
                }

            }

            for (int submarine = 0; submarine < 3; submarine++)
            {
                boatNum++;
                while (!this.tryPlace(randomX, randomY, randomDir, 3, boatNum))
                {
                    randomX = random.Next(0, 10);
                    randomY = random.Next(0, 10);
                    randomDir = random.Next(0, 2);
                }

            }

            for(int patrol = 0; patrol < 4; patrol++)
            {
                boatNum++;
                while(!this.tryPlace(randomX, randomY, randomDir, 2, boatNum))
                {
                    randomX = random.Next(0, 10);
                    randomY = random.Next(0, 10);
                    randomDir = random.Next(0, 2);
                }

            }

        }

        public bool tryPlace(int x, int y, int dir, int length, int boatNum)
        {
            for (int i = 0; i < length; i++)
            {
                switch (dir)
                {
                    case 0:
                        if (this.theBoard[x + i, y] != 0 || (x+i) >= 9)
                            return false;
                        break;
                    case 1:
                        if (this.theBoard[x, y + i] != 0 || (y + i) >= 9)
                            return false;
                        break;
                }
            }
            for (int i = 0; i < length; i++)
            {
                switch (dir)
                {
                    case 0:
                        this.theBoard[x + i, y] = boatNum;
                        break;
                    case 1:
                        this.theBoard[x, y + i] = boatNum;
                        break;
                }
            }
            return true;
        }

        public int boardValue(int x, int y)
        {
            try
            {
                return theBoard[x, y];
            }
            catch (IndexOutOfRangeException e)
            {
                return -1;
            }
        }

        public int[,] wholeBoard()
        {
            return this.theBoard;
        }

        /*
         * takes the board coordinates
         * returns if it is a hit or not
         * will update the board if necessary to display hit or miss
         * */
        public bool firedAt(int x, int y, Display window)
        {
            try{
                switch (boardValue(x, y))
                {
                        //0 = empty space. update with a splash.
                        //1 = splash
                        //2 = hit
                    case 0: 
                        this.theBoard[x, y] = 1;
                        break;
                        //3+ = a ship piece.
                    case 1:
                        return false;
                    case 2:
                        return false;
                    default:
                        int ship = this.theBoard[x, y];
                        this.theBoard[x, y] = 2;

                        if (this.CheckDestroyed(ship))
                            window.sankMy(ship);
                        
                        break;
                }
            }catch(IndexOutOfRangeException e)
            {
                return false;
            }
            return true;
        }

        private bool CheckDestroyed(int ship)
        {

            for (int x = 0; x < 10; x++)
                for (int y = 0; y < 10; y++)
                    if (theBoard[x, y] == ship)
                        return false;

            return true;
             
        }

        public bool piecesLeft()
        {
            for( int x = 0; x < 10; x++)
                for (int y = 0; y < 10; y++)
                    if (theBoard[x, y] >= 3)
                        return true;

            return false;
        }



    }
}
