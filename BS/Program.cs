﻿using SFML.Graphics;
using SFML.Window;

namespace BS
{
    public static class Program
    {
        private static readonly Color CornflowerBlue = new Color(100, 149, 237);

        public static void Main(string[] args)
        {
            Display window = new Display(new VideoMode(1280, 720), "BattleShip");
            window.Closed += (sender, eventArgs) => window.Close();
            

            while (window.IsOpen())
            {
                int winner = 0;
                Game newGame = new Game();
                GameBoard playerBoard = new GameBoard(0);
                GameBoard enemyBoard = new GameBoard(1);
                window.Clear(CornflowerBlue);
                window.showBoard(playerBoard, 0);
                window.showBoard(enemyBoard, 1);
                window.Display();
                while (winner == 0 & window.IsOpen())
                {
                    

                    // TODO: Insert Update Code Here
                   // window.Clear(CornflowerBlue);
                   // window.Display();
                    
                    // TODO: Insert Draw Code Here

                    newGame.playerTurn(window, enemyBoard);

                    window.Clear(CornflowerBlue);
                    window.showBoard(enemyBoard, 1);
                    window.showBoard(playerBoard, 0);
                    window.displayMessage();
                    window.Display();


                    newGame.enemyTurn(window, playerBoard);

                    window.Clear(CornflowerBlue);
                    window.showBoard(enemyBoard, 1);
                    window.showBoard(playerBoard, 0);
                    window.displayMessage();
                    window.Display();
                    winner = newGame.checkWinner(playerBoard, enemyBoard);
                }

                window.Display();
                window.endGame(winner);

                while (window.IsOpen())
                {
                    window.DispatchEvents();
                }

                if (winner == 1)
                    return;
                if (winner == 2)
                    return;

            }
        }
    }
}