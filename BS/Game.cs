﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.Window;
using System.Threading;

namespace BS
{
    class Game
    {
        
        //1 = running
        //2 = player turn
        //3 = enemy turn
        //4 = game completed.
        private int Status;
 
        public Game()
        {
            this.Status = 1;
        }

        public int getStatus()
        {
            return this.Status;
        }

        public void setStatus(int status)
        {
            this.Status = status;
        }

        /*
         * this will check for hits that are not surrounded by misses
         * this ensures that the ship is sunk
         * will probably need to add somemore logic so we can say 'x has sunk, stop firing at x'
         * will find out what is the best thing to shoot and fire at that location
         * */
        public void enemyTurn(Display Window, GameBoard gameBoard)
        {
            int[,] theBoard = gameBoard.wholeBoard();
            Console.WriteLine("Enemy's turn..... thinking");
            Thread.Sleep(250);

            Random random = new Random();
            int randomX = random.Next(0, 10);
            int randomY = random.Next(0, 10);
            while (Enumerable.Range(1,2).Contains(theBoard[randomX, randomY]))
            {
                randomX = random.Next(0, 10);
                randomY = random.Next(0, 10);
            }
            gameBoard.firedAt(randomX, randomY, Window);

        }

        public void playerTurn(Display window, GameBoard enemyBoard)
        {
            int x = 0;
            int y = 0;
            window.setMessage( string.Empty);
            do
            {
                while (!Mouse.IsButtonPressed(Mouse.Button.Left) & window.IsOpen()
                    || Mouse.GetPosition(window).Y < 100 || Mouse.GetPosition(window).Y > window.Size.Y
                    || Mouse.GetPosition(window).X < window.Size.X - 550 || Mouse.GetPosition(window).X > window.Size.X)
                {
                    window.DispatchEvents();
                    //    if (!window.IsOpen())
                    //        break;
                    //     Console.WriteLine("Waiting for player click");
                }

                Vector2i click = Mouse.GetPosition(window);

                x = click.X - 740;
                y = click.Y - 180;
                x /= 55;
                y /= 55;

            } while (!enemyBoard.firedAt(x, y, window));


        
        }

        public int checkWinner(GameBoard player1, GameBoard player2)
        {
            if (!player1.piecesLeft())
                return 1;
            if (!player2.piecesLeft())
                return 2;
            return 0;
        }
    }
}
